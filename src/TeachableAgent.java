import org.apache.commons.math.geometry.Vector3D;


public class TeachableAgent {
	
	private Vector3D location;	//agent's location stored in terms of pendaphone coor
	private double xUnitScale;
	private double yUnitScale;
	private Vector3D origin;
	
	public TeachableAgent(Vector3D locationOnGraph, double xUnit, double yUnit, Vector3D o)
	{
		location = locationOnGraph;
		xUnitScale = xUnit;
		yUnitScale = yUnit;
		origin = o;
	}
	
	public Vector3D getLocation()
	{
		return location;
	}
	
	public void setLocation( Vector3D newLocation )
	{
		Vector3D graphCoorLocation = new Vector3D(newLocation.getY() / xUnitScale, 
				(newLocation.getZ()) / yUnitScale, 0);
		//System.out.println(newLocation.getY() / xUnitScale + " : " + newLocation.getZ() / yUnitScale );
		System.out.println(graphCoorLocation.getX() + " : " + graphCoorLocation.getY());
		location = graphCoorLocation;
	}
	
	public Vector3D pendaphoneToGraphCoor(Vector3D pendaphoneCoor)
	{
		Vector3D graphCoor = new Vector3D( pendaphoneCoor.getY() / xUnitScale,
										pendaphoneCoor.getZ() / yUnitScale );

		return graphCoor;
	}
	
	/*
	 * 
	 * ADD CODE TO LISTEN FOR GEOGEBRA REQUESTS FOR CURRENT LOCATION???
	 * 
	 * */

}
